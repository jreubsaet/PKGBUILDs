# PKGBUILDs
This is just a collection of some PKGBUILDs modified by me. Currently, I have PKGBUILDs for the following packages:

* Dummy Package: package *dummy*
* GTK2 Modified: moved to [gtk2-mod](https://gitlab.com/jreubsaet/gtk2-mod)
* LibBlockDev Lite: package *libblockdev-lite*
* Xfwm4 Adwaitaish:  package *xfwm4-theme-adwaitaish*

A short description of what each package contains:

* **dummy**: provides an empty package to allow uninstallation of otherwise 'unwanted' packages without breaking Pacman's database through the (ab)use of the *provides* array.  This may cause issues when hard dependencies are uninstalled through this method.
* **gtk2-mod**: a patch to change the modification date and time format to be like GTK 3's, and modified default settings to reflect my personal modifications.
* **libblockdev-lite**: a slimmed-down version of **libblockdev** that does away with some redundant dependencies.
* **xfwm4-theme-adwaitaish**: a repack of the default Xfwm4 theme from Xfce 4.8.
